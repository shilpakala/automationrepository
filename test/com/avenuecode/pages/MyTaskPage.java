package com.avenuecode.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyTaskPage {
	WebDriver driver;
	By headerText = By.tagName("h1");
	By userWelcomeMessage = By.linkText("Welcome, Shilpa Meghanathan!");
	By newTask = By.id("new_task");
	By manageTaskButton = By.tagName("button");
	By subTaskModal = By.className("modal-content");
	By closeSubTaskButton = By.tagName("button");
	By myTaskButton = By.xpath("//span[@class='input-group-addon glyphicon glyphicon-plus']");
	
	
	
	public MyTaskPage(WebDriver driver) {
    	this.driver = driver;
    }
	
    public String getMyTaskHeading() {
    	return driver.findElement(headerText).getText();
    }
    
    public String getExpectedHeading(){
    	String welcomeMessage = driver.findElement(userWelcomeMessage).getText();
    	String userFullName = welcomeMessage.substring(9, welcomeMessage.indexOf('!'));
    	return userFullName;
    }
    
    public void addNewMyTask(String taskName) {
    	driver.findElement(newTask).sendKeys(taskName);
    	driver.findElement(newTask).sendKeys(Keys.ENTER);
    }
    
    public void addNewMyTask_Using_Button(String taskName){
    	driver.findElement(newTask).sendKeys(taskName);
    	driver.findElement(myTaskButton).click();
    }
    
    public int getTaskRowCount() {
    	int taskRowCount = 0;
    	WebElement base_table = driver.findElement(By.className("table"));
    	List<WebElement> table_rows = base_table.findElements(By.tagName("tr"));
    	taskRowCount = table_rows.size();
    	return taskRowCount;
    }
    
    public String getAppendText(){
    	String appendText;
    	WebElement base_table = driver.findElement(By.className("table"));
    	List<WebElement> table_rows = base_table.findElements(By.tagName("tr"));
    	List<WebElement> table_def = table_rows.get(table_rows.size()-1).findElements(By.tagName("td"));
    	appendText = table_def.get(1).getText();
    	return appendText;
    }
    
    public void clickManageSubTask(int index){
    	WebElement base_table = driver.findElement(By.className("table"));
    	List<WebElement> table_rows = base_table.findElements(By.tagName("tr"));
    	List<WebElement> table_def = table_rows.get(index).findElements(By.tagName("td"));
    	WebElement getManageTask = table_def.get(3);
    	getManageTask.findElement(manageTaskButton).click();
    }
    
    public WebElement getSubTaskModal(){
    	return driver.findElement(subTaskModal);
    }
}
