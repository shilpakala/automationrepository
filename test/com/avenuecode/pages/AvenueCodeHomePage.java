package com.avenuecode.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
 
public class AvenueCodeHomePage {
 
	WebDriver driver;
	By myTask = By.linkText("My Tasks");
    By homePageUserName = By.xpath("//table//tr[@class='heading3']");
 
    public AvenueCodeHomePage(WebDriver driver) {
    	this.driver = driver;
    }
    
    public void clickMyTask() {
    	driver.findElement(myTask).click();
    	//return driver.findElement(headerText).getText();
    } 

    public String getHomePageDashboardUserName() {
    	return driver.findElement(homePageUserName).getText();
    }
}