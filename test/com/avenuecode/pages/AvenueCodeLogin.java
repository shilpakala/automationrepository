package com.avenuecode.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
 
public class AvenueCodeLogin {
 
    WebDriver driver;
    By userName = By.id("user_email");
    By password = By.id("user_password");
    By signIn = By.linkText("Sign In");
    //By titleText =By.className("barone");
    By login = By.name("commit");
 
    public AvenueCodeLogin(WebDriver driver) {
    	this.driver = driver;
    }
 
    public void setUserName(String strUserName) {
        driver.findElement(userName).sendKeys(strUserName);;
    }
 
    public void setPassword(String strPassword) {
        driver.findElement(password).sendKeys(strPassword);
    }
 
    public void clickLogin() {
    	driver.findElement(login).click();
    }
    
    public void clickSignIn() {
    	driver.findElement(signIn).click();
    }

    /*public String getLoginTitle(){
    	return driver.findElement(titleText).getText();
    }*/
 
    public void loginToAvenueCode(String strUserName,String strPassword){
        this.clickSignIn();
    	this.setUserName(strUserName);
        this.setPassword(strPassword);
        this.clickLogin();        
    }
}
