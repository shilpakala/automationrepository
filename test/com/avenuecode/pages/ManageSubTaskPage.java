package com.avenuecode.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManageSubTaskPage {
	WebDriver driver;
	
	By MyTaskDesc = By.tagName("textarea");
	By newSubTask = By.id("new_sub_task");
	By dueDate = By.id("dueDate");
	By addButton = By.id("add-subtask");
	By modalClassName = By.className("modal-content");
	
	
	public ManageSubTaskPage(WebDriver driver) {
    	this.driver = driver;
    }
	
      
    public boolean get_MyTask_Desc_isDisabled(){
    	boolean b = (!(driver.findElement(By.tagName("textarea"))).isEnabled());
    	return b;
    }
    
    public void addNewSubTask(String subTaskName, String date) {
    	driver.findElement(newSubTask).sendKeys(subTaskName);
    	driver.findElement(dueDate).clear();
    	driver.findElement(dueDate).sendKeys(date);
    	driver.findElement(addButton).click();
    }
    
    public int getSubTaskRowCount() {
    	int subTaskRowCount = 0;
    	WebElement modalClass = driver.findElement(modalClassName);
    	WebElement base_table = modalClass.findElement(By.className("table"));
    	List<WebElement> table_rows = base_table.findElements(By.tagName("tr"));
    	
    	subTaskRowCount = (table_rows.size())-1;
    	return subTaskRowCount;
    }
    
    public String getAppendText(){
    	String appendText;
    	WebElement base_table = driver.findElement(By.className("table"));
    	List<WebElement> table_rows = base_table.findElements(By.tagName("tr"));
    	List<WebElement> table_def = table_rows.get(table_rows.size()-1).findElements(By.tagName("td"));
    	appendText = table_def.get(1).getText();
    	return appendText;
    }
}
