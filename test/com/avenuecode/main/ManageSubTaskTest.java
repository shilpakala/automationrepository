package com.avenuecode.main;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.avenuecode.pages.AvenueCodeHomePage;
import com.avenuecode.pages.AvenueCodeLogin;
import com.avenuecode.pages.ManageSubTaskPage;
import com.avenuecode.pages.MyTaskPage;

public class ManageSubTaskTest {
	WebDriver driver;
	
	AvenueCodeLogin objLogin;
	AvenueCodeHomePage objHomePage;
	MyTaskPage objMyTaskPage;
	ManageSubTaskPage objSubTaskPage;
	
	String strUserName = "mshilpakala09@gmail.com";
    String strPassword = "avenuetest";
	String validSubTaskName = "Valid Test Sub Task";
	String validDate = "08/08/2015";
	String subtaskName_LessThan_3_Char = "12";
	String invalidDate = "aaa";
	String subtaskName_MoreThan_250_Char = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. "+
			"Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. "+
			"Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia";
	String emptySubTaskName = "";
	String emptyDate = "";
	int taskIndexToTest = 1;
		
	@BeforeTest
    public void setup(){
		driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://qa-test.avenuecode.com/");
       
        objHomePage = new AvenueCodeHomePage(driver);
        objMyTaskPage = new MyTaskPage(driver);
        objLogin = new AvenueCodeLogin(driver);
        
        objLogin.loginToAvenueCode(strUserName, strPassword);
        objHomePage.clickMyTask();
        objMyTaskPage.clickManageSubTask(taskIndexToTest);
       
    }
	
	@Test(priority = 0)
	public void test_MyTask_Description_NotEditable(){
		objSubTaskPage = new ManageSubTaskPage(driver);
		Assert.assertTrue(objSubTaskPage.get_MyTask_Desc_isDisabled());
	}
	
	@Test(priority=1)
	public void test_Sub_Task_Add_Valid_New_Task() {
		objSubTaskPage = new ManageSubTaskPage(driver);
		int row_count_before = objSubTaskPage.getSubTaskRowCount();
		objSubTaskPage.addNewSubTask(validSubTaskName, validDate);
		int row_count_after = objSubTaskPage.getSubTaskRowCount();
		Assert.assertTrue((row_count_before + 1) == row_count_after);
		
    }
	
	@Test(priority=2)
	public void test_SubTask_Add_Valid_New_Task_Append() {
		objSubTaskPage = new ManageSubTaskPage(driver);
		objSubTaskPage.addNewSubTask(validSubTaskName, validDate);
		String appendedSubTask = objSubTaskPage.getAppendText();
		Assert.assertTrue(validSubTaskName.equals(appendedSubTask));
    }
	
	@Test(priority=3)
	public void test_SubTask_Add_InValid_New_Task_Min_Char() {
		objSubTaskPage = new ManageSubTaskPage(driver);
		int row_count_before = objSubTaskPage.getSubTaskRowCount();
		objSubTaskPage.addNewSubTask(subtaskName_LessThan_3_Char, validDate);
		int row_count_after = objSubTaskPage.getSubTaskRowCount();
		Assert.assertTrue((row_count_before) == row_count_after);
    }
	
	@Test(priority=4)
	public void test_SubTask_Add_InValid_New_Task_Max_Char() {
		objSubTaskPage = new ManageSubTaskPage(driver);
		int row_count_before = objSubTaskPage.getSubTaskRowCount();
		objSubTaskPage.addNewSubTask(subtaskName_MoreThan_250_Char, validDate);
		int row_count_after = objSubTaskPage.getSubTaskRowCount();
		Assert.assertTrue((row_count_before) == row_count_after);
    }
	
	@Test(priority=5)
	public void test_SubTask_Add_InValid_New_Task_NoValue() {
		objSubTaskPage = new ManageSubTaskPage(driver);
		int row_count_before = objSubTaskPage.getSubTaskRowCount();
		objSubTaskPage.addNewSubTask(emptySubTaskName, emptyDate);
		int row_count_after = objSubTaskPage.getSubTaskRowCount();
		Assert.assertTrue((row_count_before) == row_count_after);
		
    }
	
	@Test(priority=6)
	public void test_SubTask_Add_InValid_New_Task_InvalidDate() {
		objSubTaskPage = new ManageSubTaskPage(driver);
		int row_count_before = objSubTaskPage.getSubTaskRowCount();
		objSubTaskPage.addNewSubTask(validSubTaskName, invalidDate);
		int row_count_after = objSubTaskPage.getSubTaskRowCount();
		Assert.assertTrue((row_count_before) == row_count_after);
    }
	
	@AfterTest
	public void closeBrowser(){
		driver.close();
		driver.quit();
	}

}
