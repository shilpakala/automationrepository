package com.avenuecode.main;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.avenuecode.pages.AvenueCodeHomePage;
import com.avenuecode.pages.AvenueCodeLogin;

public class LoginTest {
	WebDriver driver;
	AvenueCodeLogin objLogin;
    AvenueCodeHomePage objHomePage;
    
  	
	@BeforeTest
    public void setup(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://qa-test.avenuecode.com/");
    }
	
	@Test(priority=0)
    public void test_Home_Page() {
		objLogin = new AvenueCodeLogin(driver);
		objLogin.loginToAvenueCode("mshilpakala09@gmail.com", "avenuetest");
    }
	
	@AfterTest
	public void closeBrowser(){
		driver.close();
		driver.quit();
	}
}
