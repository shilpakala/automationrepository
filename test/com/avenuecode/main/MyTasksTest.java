package com.avenuecode.main;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.avenuecode.pages.AvenueCodeHomePage;
import com.avenuecode.pages.AvenueCodeLogin;
import com.avenuecode.pages.ManageSubTaskPage;
import com.avenuecode.pages.MyTaskPage;

public class MyTasksTest {
	WebDriver driver;
	
	ManageSubTaskPage objManageSubTaskPage;
	AvenueCodeLogin objLogin;
    AvenueCodeHomePage objHomePage;
    MyTaskPage objMyTaskPage;
    ManageSubTaskPage objSubTaskPage;
    
    String strUserName = "mshilpakala09@gmail.com";
    String strPassword = "avenuetest";
    String validTaskName = "Valid Test Task";
	String taskName_LessThan_3_Char = "12";
	String taskName_MoreThan_250_Char = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. "+
			"Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. "+
			"Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia";
	String taskName_empty = "";
	int taskIndexToTest = 1;
    
	
	@BeforeTest
    public void setup(){
		driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://qa-test.avenuecode.com/");
        objLogin = new AvenueCodeLogin(driver);
        objLogin.loginToAvenueCode(strUserName, strPassword);
    }
	
	
	@Test(priority=0)
	public void test_My_Task_Heading_Text() {
		objHomePage = new AvenueCodeHomePage(driver);
		objHomePage.clickMyTask();
		objMyTaskPage = new MyTaskPage(driver);
		String actualHeading = objMyTaskPage.getMyTaskHeading();
		String expectedHeading = objMyTaskPage.getExpectedHeading();
		Assert.assertTrue(actualHeading.equals(" ​‘Hey  " + expectedHeading + ", this is your todo list for today:’"));
    }
	
	@Test(priority=1)
	public void test_My_Task_Add_Valid_New_Task_Enter_Key() {
		objMyTaskPage = new MyTaskPage(driver);
		int row_count_before = objMyTaskPage.getTaskRowCount();
		objMyTaskPage.addNewMyTask(validTaskName);
		int row_count_after = objMyTaskPage.getTaskRowCount();
		Assert.assertTrue((row_count_before + 1) == row_count_after);
		
    }
	
	@Test(priority=2)
	public void test_My_Task_Add_Valid_New_Task_Using_Button() {
		objMyTaskPage = new MyTaskPage(driver);
		int row_count_before = objMyTaskPage.getTaskRowCount();
		objMyTaskPage.addNewMyTask_Using_Button(validTaskName);
		int row_count_after = objMyTaskPage.getTaskRowCount();
		Assert.assertTrue((row_count_before + 1) == row_count_after);
		
    }
	
	@Test(priority=3)
	public void test_My_Task_Add_Valid_New_Task_Append() {
		objMyTaskPage = new MyTaskPage(driver);
		String appendedTask = objMyTaskPage.getAppendText();
		Assert.assertTrue(validTaskName.equals(appendedTask));
    }
	
	@Test(priority=4)
	public void test_My_Task_Add_InValid_New_Task_Min_Char() {
		objMyTaskPage = new MyTaskPage(driver);
		int row_count_before = objMyTaskPage.getTaskRowCount();
		objMyTaskPage.addNewMyTask(taskName_LessThan_3_Char);
		int row_count_after = objMyTaskPage.getTaskRowCount();
		Assert.assertTrue((row_count_before) == row_count_after);
    }
	
	@Test(priority=5)
	public void test_My_Task_Add_InValid_New_Task_Max_Char() {
		objMyTaskPage = new MyTaskPage(driver);
		int row_count_before = objMyTaskPage.getTaskRowCount();
		  
		objMyTaskPage.addNewMyTask(taskName_MoreThan_250_Char);
		int row_count_after = objMyTaskPage.getTaskRowCount();
		Assert.assertTrue((row_count_before) == row_count_after);
    }
	
	@Test(priority=6)
	public void test_My_Task_Add_InValid_New_Task_Empty() {
		objMyTaskPage = new MyTaskPage(driver);
		int row_count_before = objMyTaskPage.getTaskRowCount();
		objMyTaskPage.addNewMyTask(taskName_empty);
		int row_count_after = objMyTaskPage.getTaskRowCount();
		Assert.assertTrue((row_count_before) == row_count_after);
    }
	
	@Test(priority=7)
	public void test_Open_Manage_Sub_Task() {
		objMyTaskPage = new MyTaskPage(driver);
		objMyTaskPage.clickManageSubTask(taskIndexToTest);
		Assert.assertTrue(objMyTaskPage.getSubTaskModal().isDisplayed());
    }
	
	@AfterTest
	public void closeBrowser(){
		driver.close();
		driver.quit();
	}
}
